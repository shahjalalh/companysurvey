<?php

namespace Company\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints\Collection;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity(repositoryClass="Company\SurveyBundle\Entity\QuestionRepository")
 */
class Question
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var array
     *
     * @ORM\Column(name="question", type="string", length=255)
     */
    private $question;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=10)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="Answer", mappedBy="question", cascade={"persist", "remove"})
     *
     */
    private $answers;

    public function __construct(){
        $this->answers = new ArrayCollection();
    }

    public function __toString(){
        return $this->question;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param array $question
     * @return Question
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return array 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Question
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set answers
     *
     * @param Collection $answers
     * @return Question
     */
    public function setAnswers(Collection $answers)
    {
        $this->answers = $answers;

        return $this;
    }

    /**
     * Get answers
     *
     * @return string
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Add answers
     *
     * @param Answer $answer
     * @return Question
     */
    public function addAnswer(Answer $answer)
    {
        //$this->answers[] = $answers;

        if(!$this->answers->contains($answer)){
            $answer->setQuestion($this);
            $this->answers->add($answer);
        }

        return $this->answers;
    }

    /**
     * Remove answer
     *
     * @param Answer $answer
     * @return Question
     */
    public function removeAnswer(Answer $answer)
    {

        if($this->answers->contains($answer)){
            $this->answers->removeElement($answer);
        }

        return $this->answers;
    }


}
