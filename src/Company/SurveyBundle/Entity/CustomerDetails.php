<?php

namespace Company\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * CustomerDetails
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Company\SurveyBundle\Entity\CustomerDetailsRepository")
 * @UniqueEntity(
 *      fields={"email"},
 *      message="This email address already used")
 */
class CustomerDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="receivemail", type="integer")
     */
    private $receivemail;

    /**
     * @var integer
     *
     * @ORM\Column(name="ppolicy", type="integer")
     */
    private $ppolicy;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CustomerDetails
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return CustomerDetails
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set receivemail
     *
     * @param integer $receivemail
     * @return CustomerDetails
     */
    public function setReceivemail($receivemail)
    {
        $this->receivemail = $receivemail;

        return $this;
    }

    /**
     * Get receivemail
     *
     * @return integer 
     */
    public function getReceivemail()
    {
        return $this->receivemail;
    }

    /**
     * Set ppolicy
     *
     * @param integer $ppolicy
     * @return CustomerDetails
     */
    public function setPpolicy($ppolicy)
    {
        $this->ppolicy = $ppolicy;

        return $this;
    }

    /**
     * Get ppolicy
     *
     * @return integer 
     */
    public function getPpolicy()
    {
        return $this->ppolicy;
    }
}
