<?php

namespace Company\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Answer
 *
 * @ORM\Table(name="answer")
 * @ORM\Entity(repositoryClass="Company\SurveyBundle\Entity\AnswerRepository")
 */
class Answer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="string", length=255)
     */
    private $answer;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Company\SurveyBundle\Entity\Question", inversedBy="answers")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     *
     */
    private $question;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return Answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer()
    {
        return $this->answer;
    }



    /**
     * Set question
     *
     * @param \Company\SurveyBundle\Entity\Question $question
     * @return Answer
     */
    public function setQuestion($question)
    {

        if(!$question instanceof Question && $question !== null){
            throw new \InvalidArgumentException('$question must be an instance of Question or null');
        }

        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \Company\SurveyBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }
}
