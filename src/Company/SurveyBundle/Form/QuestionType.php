<?php

namespace Company\SurveyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class QuestionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('question')
            ->add('type', 'choice', array(
                'choices' => array('checkbox' => 'Checkbox', 'radio' => 'Radio'),
                'required' => true,
                'multiple' => false,
                'placeholder'=> 'Choose question type!'
            ))
            ->add('answers', 'collection', array(
                'type'=> new AnswerType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Company\SurveyBundle\Entity\Question'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'company_surveybundle_question';
    }
}
