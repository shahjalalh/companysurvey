<?php
/**
 * Created by PhpStorm.
 * User: shahjalal
 * Date: 4/22/15
 * Time: 3:48 PM
 */

namespace Company\SurveyBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CustomerAnswerType extends AbstractType{


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder->add('question');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'company_surveybundle_survey';
    }

} 