<?php

namespace Company\SurveyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CustomerDetailsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('label' => false))
            ->add('email', 'email', array('label' => false))
            ->add('receivemail', 'checkbox', array(
                'label'=>false,
                'required'=>false
            ))
            ->add('ppolicy', 'checkbox', array(
                'label'=>'I have read and agree to the privacy policy  ',
                'required'=>true
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Company\SurveyBundle\Entity\CustomerDetails'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'company_surveybundle_customerdetails';
    }
}
