<?php

namespace Company\SurveyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('CompanySurveyBundle:Default:index.html.twig');
    }
}
