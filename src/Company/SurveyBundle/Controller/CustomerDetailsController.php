<?php

namespace Company\SurveyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Company\SurveyBundle\Entity\CustomerDetails;
use Company\SurveyBundle\Form\CustomerDetailsType;

/**
 * CustomerDetails controller.
 *
 */
class CustomerDetailsController extends Controller
{

    /**
     * Lists all CustomerDetails entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CompanySurveyBundle:CustomerDetails')->findAll();

        return $this->render('CompanySurveyBundle:CustomerDetails:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new CustomerDetails entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new CustomerDetails();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('customerdetails_show', array('id' => $entity->getId())));
        }

        return $this->render('CompanySurveyBundle:CustomerDetails:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a CustomerDetails entity.
     *
     * @param CustomerDetails $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CustomerDetails $entity)
    {
        $form = $this->createForm(new CustomerDetailsType(), $entity, array(
            'action' => $this->generateUrl('customerdetails_create'),
            'method' => 'POST',
            'attr' => array('role' => 'form'),
        ));

        $form->add('submit', 'submit', array('label' => 'Go Next'));

        return $form;
    }

    /**
     * Displays a form to create a new CustomerDetails entity.
     *
     */
    public function newAction()
    {
        $entity = new CustomerDetails();
        $form   = $this->createCreateForm($entity);

        return $this->render('CompanySurveyBundle:CustomerDetails:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a CustomerDetails entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CompanySurveyBundle:CustomerDetails')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CustomerDetails entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CompanySurveyBundle:CustomerDetails:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing CustomerDetails entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CompanySurveyBundle:CustomerDetails')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CustomerDetails entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CompanySurveyBundle:CustomerDetails:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a CustomerDetails entity.
    *
    * @param CustomerDetails $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CustomerDetails $entity)
    {
        $form = $this->createForm(new CustomerDetailsType(), $entity, array(
            'action' => $this->generateUrl('customerdetails_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing CustomerDetails entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CompanySurveyBundle:CustomerDetails')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CustomerDetails entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('customerdetails_edit', array('id' => $id)));
        }

        return $this->render('CompanySurveyBundle:CustomerDetails:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a CustomerDetails entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CompanySurveyBundle:CustomerDetails')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CustomerDetails entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('customerdetails'));
    }

    /**
     * Creates a form to delete a CustomerDetails entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('customerdetails_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
