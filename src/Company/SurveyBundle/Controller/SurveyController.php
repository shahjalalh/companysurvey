<?php
/**
 * Created by PhpStorm.
 * User: shahjalal
 * Date: 4/22/15
 * Time: 12:29 PM
 */

namespace Company\SurveyBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Company\SurveyBundle\Entity\Answer;
use Company\SurveyBundle\Entity\Question;
use Company\SurveyBundle\Entity\CustomerAnswer;
use Company\SurveyBundle\Form\QuestionType;

class SurveyController extends Controller {

    public function indexAction(){

        $questions = array();
        $QuestionAndAnswers = array();


        $em = $this->getDoctrine()->getManager();

        //Getting all the questions and answers from the table
        $questionsEntity = $em->getRepository('CompanySurveyBundle:Question')->findAll();
        //$answersEntity = $em->getRepository('CompanySurveyBundle:Answer')->findAll();

        //$questionTypeEntity = $em->getRepository('CompanySurveyBundle:Question')->findById($answersEntity[1]->getQuestion());



        //echo $entities->getAnswer();

        //$this->pr($entities[1]->getQuestion()->getQuestion());
        //$this->pr($questionTypeEntity[0]->getType());

        /*
        foreach($answers as $answer){

            if(!array_key_exists($answers->getQuestion()->getId(), $QuestionAndAnswers)){
                $QuestionAndAnswers[] = array($answer->getQuestion()->getId() => $answers->getQuestion()->getQuestion());
            }

            //$QuestionAndAnswers[] = $entity->getQuestion()->getId();

            //$this->pr($entity);
            echo "------";
        }
        */
        //$this->pr($questionsEntity);
        //$this->pr($entities);
        //exit;

        return $this->render('CompanySurveyBundle:Survey:index.html.twig', array(
            'questions' => $questionsEntity
        ));
    }

    /**
     * Creates a new Question entity.
     *
     */
    public function createAction(Request $request)
    {

        //$this->pr($request->request->get("http://localhost/companysurvey/web/app_dev.php/survey/create"));
        $submittedUrl = $_SERVER['REQUEST_URI'];

        $resultString = explode("?", $submittedUrl);
        $questionAnswers = explode("&", $resultString[1]);
        //$this->pr($questionAnswers);

        $em = $this->getDoctrine()->getManager();


        //for loop on $questionAnswers
        foreach($questionAnswers as $queAns){

            $customerAnswerEntity = new CustomerAnswer();
            
            $questionAnswer = explode("=", $queAns);

            $customerAnswerEntity->setQuestion($questionAnswer[0]);
            $customerAnswerEntity->setAnswer($questionAnswer[1]);
            //$customerAnswerEntity->setCreatedAt();
            //for loop end


            $em->persist($customerAnswerEntity);
            $em->flush();
        }


        //$this->pr($customerAnswerEntity);

        //exit;


        return $this->redirect($this->generateUrl('survey_greeting'));
    }

    public function greetingAction(){

        return $this->render('CompanySurveyBundle:Survey:greeting.html.twig');

    }


    /*
     * Debug method. Delete before hosting
     */
    private function pr($var){
        echo '<pre>';
        \Doctrine\Common\Util\Debug::dump($var);
        echo '</pre>';
    }
} 